@ECHO OFF
setlocal enabledelayedexpansion
ECHO Class + RGB merge
ECHO Remove black backgroud color and save BIL to ECW
ECHO Date: 22-09-2019
ECHO Version: 1.0
ECHO QGIS 2.14

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET PYTHON=C:\Users\m.szczepkowski\AppData\Local\Programs\Python\Python37\Lib\site-packages\osgeo\scripts


REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_BG

REM Path to working dir
SET WORK=%cd%
if not exist "%WORK%\_BG" mkdir "%WORK%\_BG"

REM COUNTER FILES
dir /b RGB\*.bil 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.bil"') DO (
    ECHO.
	ECHO Processing  %%i    !Counter! / %count% FILES
	REM merge and cut if exists coverage
	
	
	gdalwarp -dstnodata "0 0 0" -dstalpha -of GTiff -ot Byte -co "TFW=YES" -co "COMPRESS=LZW" "%WORK%\%%i" "%WORK%\_BG\%%~ni%OUT_SUFFIX%.ecw"
	set /A Counter+=1
	
)
ECHO.
ECHO Done
PAUSE